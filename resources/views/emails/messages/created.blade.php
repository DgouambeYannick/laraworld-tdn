@component('mail::message')
# Hey {{$name}}

-{{$name}}
-{{$email}}
-{{$commentaire}}

@component('mail::button', ['url' => ''])
More info
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
