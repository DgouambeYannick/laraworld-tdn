<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{pages_title(isset($title) ? $title : '')}}</title>

    {{--<title>Laracarte - {{isset($title) ? $title : ''}}</title>
--}}
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/font-awesome.css')}}">
    <!-- Styles -->
</head>
<body>

@include('layouts.partials.header')
@yield('content')
@include('layouts.partials.footer')

<script crossorigin="anonymous" src="{{asset('/js/jquery.min.js')}}"></script>
<script crossorigin="anonymous" src="{{asset('/js/bootstrap.min.js')}}"></script>
@yield('scripts.footer')
@include('flashy::message')
</body>
</html>