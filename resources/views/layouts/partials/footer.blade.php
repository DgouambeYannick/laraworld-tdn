<footer class="text-center">
    <p>&copy; {{date('Y')}} &middot; {{config('app.name')}} by <a href="https://gitlab.com/DgouambeYannick">@dgouambe</a> - Une application clonée de Laracarte.
    </p>
    <strong>Cette application a été construite pour l'apprentissage de Laravel.</strong>
</footer>