<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navgation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{route('home')}}" class="navbar-brand">{{config('app.name')}}</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{pages_active('home')}}">
                    <a href="{{route('home')}}">Acueil</a>
                </li>
                <li class="{{pages_active('about_uri')}}">
                    <a href="{{route('about_uri')}}">A propos</a>
                </li>
                <li class="{{pages_active('artisan_uri')}}">
                    <a href="{{route('artisan_uri')}}">Artisans</a>
                </li>
                <li class="dropdown">
                    <a aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" role="button" href="#" class="dropdown-toggle">
                        Planète <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="https://laravel.com">Laravel.com</a></li>
                        <li><a href="https://laravel.io">Laravel.io</a></li>
                        <li><a href="https://laracasts.com">Laracasts</a></li>
                        <li><a href="https://larajobs.com">Larajobs</a></li>
                        <li><a href="https://laravel-news.com">Laravel News</a></li>
                        <li><a href="https://larachat.com">Larachat</a></li>
                       {{-- <li role="separator" class="divider"></li>
                        <li class="dropdown-header">LAra</li>
                        <li><a href="#">Laravel news</a></li>
                        <li><a href="#">Laravel geek</a></li>--}}
                    </ul>
                </li>
                <li class="{{pages_active('contact_uri')}}">
                    <a href="{{route('contact_uri')}}">Contact</a>
                </li>
            </ul>
            <ul class="nav navbar-right navbar-nav">
                <li><a href="#">S'identfier</a></li>
                <li><a href="#">Inscription</a></li>
            </ul>
        </div>
    </div>
</nav>