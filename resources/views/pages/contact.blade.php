@extends("layouts.app",['title'=>'Contact'])
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <h1>Get In Touch</h1>
                <p>If you having trouble with this service. please <a href="mailto:{{email_support()}}">ask for help</a></p>
                <form action="{{route('contact.store')}}" method="POST">
                    {{csrf_field()}}
                    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                        <label for="name" class="sr-only label control-label"></label>
                        <input value="{{old('name')?old('name'):''}}" type="text" id="name" class="form-control"
                               name="name" placeholder="Votre Nom"/>
                        {!!$errors->first('name','<span class="alert-danger help-block">:message</span>')!!}
                    </div>
                    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                        <label for="email" class="sr-only label control-label"></label>
                        <input value="{{old('email')?old('email'):''}}" type="email" id="email" class="form-control"
                               name="email" placeholder="Votre Email"/>
                        {!!$errors->first('email','<span class="alert-danger help-block">:message</span>')!!}
                    </div>
                    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                        <textarea value="{{old('commentaire')?old('commentaire'):''}}" name="commentaire" id=""
                                  cols="30" rows="10" class="form-control"
                                  placeholder="Laissez un commentaire"></textarea>
                        {!!$errors->first('commentaire','<span class="alert-danger help-block">:message</span>')!!}
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control" value="Envoyer"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop