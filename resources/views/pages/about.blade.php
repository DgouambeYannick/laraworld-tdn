@extends("layouts.app",['title'=>'A propos'])
@section('content')
    <div class="container">
        <h2>Qu'est-ce que {{config('app.name')}} ?</h2>
        <p>{{config('app.name')}} est une application clone de <a href="https://laracarte.herokuapp.com/">laracarte</a></p>
        <div class="row">
            <div class="col-md-6">
                <p class="alert alert-warning">
                    <i class="fa fa-warning"></i><strong> Cette application a été construite par <a href="https://gitlab.com/DgouambeYannick">@dgouambe</a> pour apprendre
                    </strong>
                </p>
            </div>
        </div>

        <p>N'hésitez pas à aider à améliorer le <a href="https://gitlab.com/DgouambeYannick/laraworld-tdn">source code</a></p>

        <hr>

        <h2>Qu'est-ce que laracarte ?</h2>
        <p>laracarte est le site Web qui a inspiré {{config('app.name')}} was inspired :)</p>
        <p>Plus d'infos <a href="https://laracarte.herokuapp.com/about">here</a></p>

        <hr>

        <h2>Quels outils et services sont utilisés dans {{config('app.name')}} ?</h2>
        <p>Fondamentalement, il est construit sur Laravel & Bootstrap. Mais il y a un tas de services utilisés pour le courrier électronique et d'autres sections.</p>
        <ul>
            <li>Laravel</li>
            <li>Bootstrap</li>
            <li>Amazon S3</li>
            <li>Mandrill</li>
            <li>Google Maps</li>
            <li>Gravatar</li>
            <li>Antony Martin's Geolocation Package</li>
            <li>Michel Fortin's Markdown Parser Package</li>
            <li>Heroku</li>
        </ul>
    </div>
    </div>
    </div>
@stop