<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Mail\ContactMessageCreated;

Route::get('/', 'PagesController@home')->name('home');
Route::get('/about', 'PagesController@about')->name('about_uri');
Route::get('/artisan', 'PagesController@artisan')->name('artisan_uri');
Route::get('/contact', 'PagesController@contact')->name('contact_uri');
Route::post('/contacts', 'ContactController@store')->name('contact.store');
Route::get('/testmail', function (){
    return new ContactMessageCreated('DGOUAMBE','fnasi@yan','terminé');
});