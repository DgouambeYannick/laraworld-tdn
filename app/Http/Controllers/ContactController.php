<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Mail\ContactMessageCreated;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * @param ContactFormRequest $request
     * @return mixed
     */
    function store(ContactFormRequest $request){
        //sleep(3);
        $message = Message::firstOrCreate([
           'name'=>$request->name,
           'email'=>$request->email,
           'commentaire'=>$request->commentaire
        ]);

        Mail::to(email_support())
             ->send(new ContactMessageCreated($message));
        flashy()->success('Nous vous répondrons dans les plus bref délais!!');

        return redirect(route('home'));
    }
}
