<?php

namespace App\Http\Controllers;

use App\Models\Message;
use ContactFormRequest;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    function home(){
        return view('welcome');
    }
    function about(){
        return view('pages.about');
    }
    function artisan(){
        return view('pages.artisans');
    }
    function contact(){
        return view('pages.contact');
    }

}
