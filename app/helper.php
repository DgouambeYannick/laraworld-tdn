<?php
/**
 * Created by PhpStorm.
 * User: DGOUAMBE
 * Date: 21/02/2019
 * Time: 19:39
 */

use Illuminate\Support\Facades\Route;

if(!function_exists(('pages_title'))){
    function pages_title($title){
        $base= config('app.name');
        if($title == ''){
            return $base;
        }else{
            return $title.' | '.$base;
        }
    }
}

if(!function_exists(('pages_active'))){
    function pages_active($route)
    {
        return Route::is($route) ? 'active' : '';
    }
}


if(!function_exists(('email_support'))){
    function email_support()
    {
        return env('EMAIL_SUPPORT');
    }
}
